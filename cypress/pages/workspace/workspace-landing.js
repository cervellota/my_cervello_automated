/// <reference types = "cypress" />

class WorkspaceLanding {
    Selectors = {
        workspaceName: '[name="name"]',
        createButton: "CREATE",
        createWorkspaceFeedback: "Your workspace has been successfully created"
    }

    addWorkSpace(workspaceName) {
        cy.get(this.Selectors.workspaceName).type(workspaceName);
        cy.contains(this.Selectors.createButton).click();
        cy.contains(this.Selectors.createWorkspaceFeedback).should("exist");
    }

}

export const workspaceLanding = new WorkspaceLanding();