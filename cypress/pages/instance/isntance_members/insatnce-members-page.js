/// <reference types = "cypress" />


class InstanceMembersPage {


Selectors = {
  addMemberButton: "Add member",
  emailField: "[type='email']",
  teamAdminSelection: "Team Admin",
  teamMateSelection: "Teammate",
  nextButton: "Next",
  teamsList: "#team",
  lastAddMemberButton: "[type='submit']",
};

  inviteNewTeamAdmin(memberEmail) {
    cy.contains("button", this.Selectors.addMemberButton).click();
    cy.get(this.Selectors.emailField).type(memberEmail);
    cy.contains("h6", this.Selectors.teamAdminSelection).click();
    cy.contains("button", this.Selectors.nextButton).click();
    cy.get(this.Selectors.teamsList).children().first().click();
    cy.get(this.Selectors.lastAddMemberButton).last().click();
  }

  inviteNewTeammate(memberEmail) {
    cy.contains("button", this.Selectors.addMemberButton).click();
    cy.get(this.Selectors.emailField).type(memberEmail);
    cy.contains("h6", this.Selectors.teamMateSelection).click();
    cy.contains("button", this.Selectors.nextButton).click();
    cy.get(this.Selectors.teamsList).children().first().click();
    cy.get(this.Selectors.lastAddMemberButton).last().click();
  }
}

export const instanceMembersPage = new InstanceMembersPage();
