/// <reference types = "cypress" />

class InstanceLandingPage {
  Selectors = {
    addInstanceAdminBtn: "Add administrator",
    manageAdminsBtn: "Manage administrators",
    emailField: "#email",
    inviteBtn: "[type='submit']",
    confirmInvitationBtn: "Ok",
    test_ResendInvBtn: "#test-resendInvBtn",
    test_RevokeInvBtn: "#test-revokeAdminBtn",
    confirmRevokeBtn: "Yes",
    addWorkspaceButton: "Add workspace" 
  };

  inviteInstanceAdmin(instanceAdminEmail) {
    cy.contains(this.Selectors.addInstanceAdminBtn).click();
    cy.get(this.Selectors.emailField).type(instanceAdminEmail);
    cy.get(this.Selectors.inviteBtn).click();
    cy.contains("span", this.Selectors.confirmInvitationBtn).click();
  }

  resendInvitationToAnInstanceAdmin() {
    cy.contains(this.Selectors.manageAdminsBtn).click();
    cy.get(this.Selectors.test_ResendInvBtn).first().click({ force: true });
  }

  revokeInstanceAdmin() {
    cy.contains(this.Selectors.manageAdminsBtn).click();
    cy.get(this.Selectors.test_RevokeInvBtn).first().click({ force: true });
    cy.get("button.danger").click({ force: true, multiple: true });
  }

  openWorkspaceDrawer() {
    cy.contains("a", this.Selectors.addWorkspaceButton).click();
  }
}

export const instanceLandingPage = new InstanceLandingPage();
