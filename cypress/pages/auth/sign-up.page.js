import * as faker from "faker";
import { Selectors } from "./auth_selectors";

export class SignUpPage {
  signUp(newRandomUserEmail) {
    cy.get(Selectors.signUpEmailField).type(newRandomUserEmail + "{enter}");
  }
}

export const signUpPage = new SignUpPage();
