/// <reference types = "cypress"/>

export class MagicLinkPage {
  verifyCheckInboxAppearance() {
    cy.contains("h4", "Check your inbox").should("be.visible");
  }
}

export const magicLinkPage = new MagicLinkPage();
