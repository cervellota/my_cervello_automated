export const Selectors = {
  usernameField: '[name="username"]',
  passwordField: '[name="password"]',
  staySignedInOption: "span .MuiSwitch-switchBase",
  loginBtn: '[type="submit"]',
  signUpEmailField: "#email",
  resendButton: "Resend"
};
