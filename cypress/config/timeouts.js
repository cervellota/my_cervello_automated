export const timeouts = {
  short: 5000,
  medium: 10000,
  long: 15000,
  xlong: 20000,
  xxlong: 25000,
};
