// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.

//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import "./commands";
/**
 *
 */
// Alternatively you can use CommonJS syntax:
// require('./commands')

/** Set multiple configuration files
 * Testing Env -> cypress.testing.json
 * Dev Env -> cypress.dev.json
 * RC Env -> cypress.rc.json
 */

// promisified fs module
// const fs = require("fs-extra");
// const path = require("path");

// function getConfigurationByFile(file) {
//   const pathToConfigFile = path.resolve("..", "config", `${file}.json`);

//   return fs.readJson(pathToConfigFile);
// }

// // plugins file
// module.exports = (on, config) => {
//   // accept a configFile value or use testing by default
//   const file = config.env.configFile || "testing";

//   return getConfigurationByFile(file);
// };

import "cypress-mochawesome-reporter/register";
import "@shelex/cypress-allure-plugin";

// load and register the grep feature
// https://github.com/cypress-io/cypress-grep
require("cypress-grep")();
