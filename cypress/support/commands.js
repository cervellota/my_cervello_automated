// ***********************************************
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************

// ***********************************************
// Common Helper methods commonly used at Cervello Tests
// commands please read more here:
// ***********************************************

import "cypress-localstorage-commands";

// const token = Cypress.env("token");
// const authorization = `Bearer ${token}`;

Cypress.Commands.add("loginByAPI", () => {
  const apiHost = Cypress.env("apiUrl");
  cy.request({
    method: "POST",
    url: `${apiHost}/auth`,
    body: {
      email: "cer01@qa.team",
      password: "Iotblue55$",
      grantType: "password",
    },
  }).then((resp) => {
    window.localStorage.setItem("access_token", resp.body.access_token);
  });
});

Cypress.Commands.add("loginByAPI2", () => {
  const apiHost = Cypress.env("apiUrl");
  cy.request({
    method: "POST",
    url: `${apiHost}/auth`,
    body: {
      username: "automation@qa.team",
      password: "Iotblue55$",
      grantType: "password",
    },
  }).then((resp) => {
    window.localStorage.setItem("access_token", resp.body.access_token);
    cy.request({
      method: "GET",
      url: `${apiHost}/instances`,
      headers: {
        authorization: resp.body.access_token,
      },
    }).then((instances) => {
      window.localStorage.setItem(
        "ownedInstnace",
        instances.body.result.instanceOwned.id
      );
      console.log(instances.body.result.instanceOwned.id);
    });
  });
});
