/// <reference types="cypress" />
/// <reference types="@shelex/cypress-allure-plugin" />
const path = require("path");
const fs = require("fs-extra");
// ***********************************************************
// This example plugins/index.js can be used to load plugins
//
// You can change the location of this file or turn off loading
// the plugins file with the 'pluginsFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/plugins-guide
// ***********************************************************

// This function is called when a project is opened or re-opened (e.g. due to
// the project's config changing)
/// <reference types="@shelex/cypress-allure-plugin" />
const allureWriter = require("@shelex/cypress-allure-plugin/writer");

module.exports = (on, config) => {
  allureWriter(on, config);
  return config;
};

/**
 * @param {--env} used to specify which environment to be used to execute the tests
 * Set the Env Variables to be called with "--env" while executing the Script
 */
const getConfigurationByFile = (file) => {
  const pathToConfigFile = `cypress/config/cypress.${file}.json`;
  // const pathToConfigFile = path.resolve("..", "config", `${file}.json`);
  // return fs.readJson(pathToConfigFile);

  return (
    !!file && fs.readJson(path.join(__dirname, "../../", pathToConfigFile))
  );
};

/**
 * @type {Cypress.PluginConfig}
 */
// eslint-disable-next-line no-unused-vars
module.exports = (on, config) => {
  allureWriter(on, config);
  const environment = config.env.configFile;
  const configEnvironment = getConfigurationByFile(environment);
  require("cypress-mochawesome-reporter/plugin")(on);
  require("cypress-grep/src/plugin")(config);

  return configEnvironment || config;

  // `on` is used to hook into various events Cypress emits
  // `config` is the resolved Cypress config
};
