/// <reference types= "cypress" />

import * as faker from "faker";
import { workspaceLanding } from "../../../pages/workspace/workspace-landing";
import { instanceLandingPage } from "../../../pages/instance/instance-landing-page";

describe("Workspace Test", () => {

    beforeEach("Login", () => {
        cy.loginByAPI();
        cy.visit("/");
    });

    it("Should create new workspace successfully", () => {
        let workspaceName = faker.internet.userName();
        instanceLandingPage.openWorkspaceDrawer();
        workspaceLanding.addWorkSpace(workspaceName)
    })
});