/// <reference types= "cypress" />

import * as faker from "faker";
import { chooseInstancePage } from "../../pages/instance/choose-instance-page";
import { instanceLandingPage } from "../../pages/instance/instance-landing-page";
import { timeouts } from "../../config/timeouts";

let ownedInstance;

describe("Instance Tests", () => {
  let listInstanceAdmins;
  beforeEach("Auth", () => {
    cy.loginByAPI2();
    ownedInstance = cy
      .getLocalStorage("ownedInstnace")
      .then((ownedInstance) => {
        listInstanceAdmins = `${Cypress.env(
          "apiUrl"
        )}/instances/${ownedInstance}/admins`;
      });
  });

  it("Should open owned instance successfully", () => {
    cy.visit("/");
    chooseInstancePage.openOwnedInstance();
  });

  it("Should list 5 stubbed Instances successfully", () => {
    cy.intercept("GET", "**/instances", { fixture: "instances.json" }).as(
      "instances"
    );
    cy.visit("/");
    cy.wait("@instances");
    cy.contains("All instances").should("contain.text", "5");
  });

  it("Should Shows 'Create Instance option' when no owned instance is created", () => {
    cy.visit("/");
    cy.intercept("GET", "**/instances", "Empty");
    cy.contains("Create your first instance").should("exist");
  });

  context("Manage Instance", () => {
    it("Should invite new Instance Admin Successfully", () => {
      cy.visit(Cypress.env("directInstanceUrl"), {
        timeout: timeouts.medium,
      });
      instanceLandingPage.inviteInstanceAdmin(faker.internet.email());
      cy.contains("Your administration has been added successfully.").should(
        "exist"
      );
    });
  });
  context("Manage Instance Admins (Invite & Revoke)", () => {
    beforeEach("Invite instance admin By API", () => {
      cy.visit(Cypress.env("directInstanceUrl"), {
        timeout: timeouts.medium,
      });

      cy.getLocalStorage("access_token").then((token) => {
        cy.request({
          method: "POST",
          url: listInstanceAdmins,
          body: {
            email: faker.internet.email(),
          },
          headers: {
            authorization: `Bearer ${token}`,
          },
        }).as("instanceAdmin");
      });
    });
    it("Should Resend Instance Invitation to a member", () => {
      instanceLandingPage.resendInvitationToAnInstanceAdmin();
      cy.contains("Invitation sent successfully").should("exist");
    });

    it("Should Revoke Instance Invitation to a member", () => {
      instanceLandingPage.revokeInstanceAdmin();
      cy.contains("Admin revoked successfully.").should("exist");
    });
  });
});
