/// <reference types= "cypress" />

import { instanceMembersPage } from "../../../pages/instance/isntance_members/insatnce-members-page";
import * as faker from "faker";

describe("Instance Members Tests", () => {
  beforeEach("Login", () => {
    cy.loginByAPI2();
  });

  it("Should invite new Team Admin from instance successfully", () => {
    cy.visit(`${Cypress.env("directInstanceUrl")}/members`);
    instanceMembersPage.inviteNewTeamAdmin(faker.internet.email());
    cy.contains("Your invitation has been sent successfully.").should("exist");
  });
  
  it("Should invite new teammate from instance successfully", () => {
    cy.visit(`${Cypress.env("directInstanceUrl")}/members`);
    instanceMembersPage.inviteNewTeammate(faker.internet.email());
    cy.contains("Your invitation has been sent successfully.").should("exist");
  });
});
