/// <reference types = "cypress"/>

const apiInstances = `${Cypress.env("apiUrl")}/instances`;
import * as faker from "faker";
let ownedInstance;

let options = {
  body: {
    email: faker.internet.email(),
    address: faker.address.city(),
  },
  url: "",
  headers: {},
  method: "GET",
};

describe("Testing Instance API", () => {
  beforeEach("Set Prerequisites before tests", () => {
    options.url = apiInstances;
    cy.fixture("resources.json").as("resource");

    cy.loginByAPI2();
    cy.getLocalStorage("access_token").then((token) => {
      options["headers"] = { authorization: `Bearer ${token}` };
    });
  });

  context("GET /instances", () => {
    it("Should list instances successfully after valid Login", () => {
      console.log(options);
      let newOptions = { ...options };
      delete newOptions["body"];
      cy.request({
        ...newOptions,
      }).then((response) => {
        expect(response.status).to.eq(200);
        expect(response.body.result).to.have.property("instanceOwned");
      });
    });
  });
});
// it("Should Invite Instance Admin Successfully", () => {
//   const token = Cypress.env("token");
//   const authorization = `Bearer ${token}`;
//   cy.request({
//     method: "POST",
//     url: `${apiInstances}/${resource.instanceId}`,
//     headers: {
//       authorization,
//     },
//   });
//   console.log(url);
// });

describe("Negative Tests for Instances AP", () => {
  it("Should respond with 401 to unauthorized user", () => {
    cy.request({
      method: "GET",
      url: apiInstances,
      failOnStatusCode: false,
    }).then((response) => {
      expect(response.status).to.eq(401);
    });
  });
});
