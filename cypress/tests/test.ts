// /// <reference types = "cypress"/>

// import * as faker from "faker";

// const pingApi = "https://api.testing.cervello.io/iam/v1/ping";
// const loginAPIUrl = `${Cypress.env("apiUrl")}/auth`;
// const registerAPIUrl = `${Cypress.env("apiUrl")}/register`;
// const userName = Cypress.env("userName");
// const password = Cypress.env("password");

// describe("Auth APIs Tests", () => {
//   it("Test Ping", () => {
//     cy.request("GET", pingApi).then((resp) => {
//       expect(resp.status).to.eq(200);
//       expect(resp.body.message).to.eq("pong");
//     });
//   });

//   context("Login Tests", () => {
//     it("Should successfully login using the API", () => {
//       cy.request({
//         method: "POST",
//         url: loginAPIUrl,
//         body: {
//           username: userName,
//           password: password,
//           grantType: "password",
//         },
//       }).then((resp) => {
//         expect(resp.status).to.eq(200);
//         expect(resp.body).to.have.any.keys("access_token");
//       });
//     });
//   });

//   it("Should get 401 for invalid username or pass", () => {
//     cy.request({
//       method: "POST",
//       url: loginAPIUrl,
//       body: {
//         username: "Invalid",
//         password: "Iotbl55$",
//         grantType: "password",
//       },
//       failOnStatusCode: false,
//     }).then((resp) => {
//       expect(resp.status).to.eq(401);
//     });
//   });

//   context("SignUp Tests", () => {
//     it("Should Send Sign-up Link Successfully when providing valid email", () => {
//       cy.request({
//         method: "POST",
//         url: registerAPIUrl,
//         body: {
//           email: faker.internet.email(),
//         },
//       })
//         .its("status")
//         .should("eq", 200);
//     });
//   });

//   it("Registration Email Validation", () => {
//     cy.request({
//       method: "POST",
//       url: registerAPIUrl,
//       body: {
//         email: "Invalid",
//       },
//       failOnStatusCode: false,
//     })
//       .its("status")
//       .should("eq", 400);

//     cy.request({
//       method: "POST",
//       url: registerAPIUrl,
//       body: {},
//       failOnStatusCode: false,
//     }).then((resp) => {
//       expect(resp.body.result).to.eq("Email is a required field");
//     });
//   });

//   context("Logout Tests", () => {
//     it("Should Logout after deleting CERVELLO_SSO from Cookies", () => {
//       // cy.intercept("GET", "**/instances").as("instances");
//       cy.loginByAPI();
//       cy.visit("/");
//       // cy.wait("@instances");
//       cy.clearCookies();
//       cy.reload();
//       cy.url().should("contain", "auth");
//     });
//   });
// });

describe("test", () => {
  beforeEach(() => {
    cy.visit("/");
  });
  it("test2", () => {
    cy.loginByAPI2();
    cy.visit("/");
  });
});
